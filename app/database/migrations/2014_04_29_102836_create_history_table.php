<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('history', function(Blueprint $table){
			$table->increments('id');
			$table->integer('project_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->enum('type', array('misc', 'project', 'milestone', 'task', 'comment'));
			$table->integer('type_id')->nullable();
			$table->string('action')->nullable();
			$table->text('content')->nullable();
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('history');
	}

}
