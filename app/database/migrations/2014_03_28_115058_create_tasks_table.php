<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function($table){
			$table->increments('id');
			$table->integer('project_id')->nullable();
			$table->integer('milestone_id')->nullable();
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('phase_id')->nullable();
			$table->integer('assigned_to')->nullable();
			$table->integer('created_by')->nullable();
			$table->dateTime('due_date')->nullable();
			$table->tinyInteger('sort')->nullable();
			$table->tinyInteger('completed')->nullable();
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
