<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        DB::unprepared('ALTER TABLE users AUTO_INCREMENT = 1;');
        User::create(array('email' => 'gilbert@pellegrom.me', 'password' => 'admin', 'name' => 'Gilbert Pellegrom', 'api_key' => '1234567891', 'role' => 'admin'));
        User::create(array('email' => 'test1@test.com', 'password' => 'test1', 'name' => 'Test 1', 'api_key' => '1234567892'));
		User::create(array('email' => 'test2@test.com', 'password' => 'test2', 'name' => 'Test 2', 'api_key' => '1234567893'));
		User::create(array('email' => 'test3@test.com', 'password' => 'test3', 'name' => 'Test 2', 'api_key' => '1234567894'));
    }

}
