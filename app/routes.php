<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::post('stripe/webhook', 'WebhookController@handleWebhook');

// Api (auth filter is done in ApiController)
Route::group(array('prefix' => 'api/1'), function()
{
	Route::get('/', function(){
		return Response::json(array(
			'error' => array(
				'code' => 404,
				'message' => 'Not found'
			)
		), 404);
	});
    Route::resource('projects', 'ProjectsController');
	Route::resource('milestones', 'MilestonesController');
	Route::resource('tasks', 'TasksController');
	Route::resource('task-comments', 'TaskCommentsController');
	Route::resource('task-comment-attachments', 'TaskCommentAttachmentsController');
	Route::resource('task-categories', 'TaskCategoriesController');
	Route::resource('task-phases', 'TaskPhasesController');
	Route::resource('users', 'UsersController');
	Route::resource('history', 'HistoryController');
});

// Is admin
Route::group(array('before' => 'auth.admin'), function()
{
	Route::controller('admin', 'AdminController');
});

// Logged in
Route::group(array('before' => 'auth'), function()
{
	Route::get('logout', array('uses' => 'AuthController@logout'));
	Route::get('start-free-trial', array('uses' => 'AuthController@startFreeTrial'));
	Route::get('downgrade-to-team', array('uses' => 'AuthController@downgradeToTeam'));
	Route::get('billing', 'BillingController@index');
	Route::get('billing/upgrade/{plan}', 'BillingController@upgrade');
	Route::post('billing/upgrade/{plan}', 'BillingController@processUpgrade');
	Route::get('billing/edit', 'BillingController@edit');
	//Route::post('billing/edit', 'BillingController@processEdit');
	Route::post('billing/cancel', 'BillingController@processCancel');
	Route::get('billing/invoice/{id}', 'BillingController@invoice');
});

// Logged in and subscribed
Route::group(array('before' => 'auth.subscribed'), function()
{
	Route::controller('account', 'AccountController');
	Route::get('team/{project_id?}', 'TeamController@index');
	Route::post('team/add-user', 'TeamController@addUser');
	Route::post('team/remove-user', 'TeamController@removeUser');
	Route::get('import/basecamp', 'ImportController@basecamp');
	Route::get('import/basecamp/login', 'ImportController@basecampLogin');
	Route::post('import/basecamp/go', 'ImportController@doImport');

	Route::get('app', function(){
		return View::make('app.main');
	});
});

// Not logged in
Route::group(array('before' => 'guest'), function()
{
	Route::get('login', array('uses' => 'AuthController@login'));
	Route::post('login', array('uses' => 'AuthController@processLogin'));
	Route::get('signup', array('uses' => 'AuthController@signup'));
	Route::post('signup', array('uses' => 'AuthController@processSignup'));
	Route::get('signup-closed', array('uses' => 'AuthController@signupClosed'));
	Route::get('signup/invite/{token}', array('uses' => 'AuthController@inviteSignup'));
	Route::post('signup/invite/{token}', array('uses' => 'AuthController@processInviteSignup'));
	Route::controller('password', 'RemindersController');

	Route::get('/', array('uses' => 'SiteController@index'));
	Route::get('/pricing', array('uses' => 'SiteController@pricing'));
	Route::get('/terms', array('uses' => 'SiteController@terms'));
	Route::get('/privacy', array('uses' => 'SiteController@privacy'));
});
