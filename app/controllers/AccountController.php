<?php

class AccountController extends Controller {

	public function getIndex()
	{
		return View::make('app.account')->with(array('user' => Auth::user(), 'page_title' => 'Manage Account'));
	}

	public function postInfo()
	{
		$user = Auth::user();
		$data = array(
			'name' => Input::get('name'),
			'email' => Input::get('email')
		);

		$validator = Validator::make($data, array(
			'name' => 'required',
			'email' => 'required|email|unique:users,email,'. $user->id
		));
		if($validator->fails()){
			return Redirect::to('account')->withErrors($validator);
		}

		if(!$user->update($data)){
			return Redirect::to('account')->withErrors($user->getErrors());
		}

		return Redirect::to('account')->withStatus('Account info saved');
	}

	public function postPassword()
	{
		$user = Auth::user();
		$data = array(
			'password' => Input::get('password'),
			'password_confirmation' => Input::get('password_confirmation')
		);

		$validator = Validator::make($data, array(
			'password' => 'required|confirmed'
		));
		if($validator->fails()){
			return Redirect::to('account')->withErrors($validator);
		}

		if(!$user->update(array('password' => $data['password']))){
			return Redirect::to('account')->withErrors($user->getErrors());
		}

		return Redirect::to('account')->withStatus('Account password changed');
	}

}
