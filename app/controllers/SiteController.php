<?php

class SiteController extends \BaseController {

	public function index()
	{
		return View::make('site.index')->with(array(
			'page_description' => 'Your todo\'s, bugs, issues and features in one place. Stop using multiple apps or your inbox to manage your software projects, use Artisan instead.'
		));
	}

	public function pricing()
	{
		return View::make('site.pricing')->with(array(
			'page_title' => 'Pricing',
			'page_description' => 'Try Artisan free for 14 days. No commitment. Change plans or cancel any time.'
		));
	}

	public function terms()
	{
		return View::make('site.terms')->with(array('page_title' => 'Terms & Conditions'));
	}

	public function privacy()
	{
		return View::make('site.privacy')->with(array('page_title' => 'Privacy Policy'));
	}

}
