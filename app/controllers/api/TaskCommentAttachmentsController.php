<?php

class TaskCommentAttachmentsController extends \ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comments = TaskComment::all();
		$comments = $comments->filter(function($comment) use ($tasks){
			return ($tasks->contains($comment->task_id));
		})->values();

		$attachments = TaskCommentAttachment::all();
		$attachments = $attachments->filter(function($attachment) use ($comments){
			return ($comments->contains($attachment->comment_id));
		})->values();

		if(Input::get('comment_id')){
			$attachments = $attachments->filter(function($attachment){
				return $attachment->comment_id == Input::get('comment_id');
			})->values();
		}

		return Response::json($attachments);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comments = TaskComment::all();
		$comments = $comments->filter(function($comment) use ($tasks){
			return ($tasks->contains($comment->task_id));
		})->values();

		if(!$comments->contains(Input::get('comment_id'))){
			return Response::json(array('code' => '412', 'message' => 'Invalid comment_id'), 412);
		}

		$attachment = new TaskCommentAttachment(Input::all());
		$attachment->uploaded_by = $this->user->id;

		if($attachment->save()){
			/*History::create(array(
				'project_id' => $attachment->comment->task->project->id,
				'user_id' => $this->user->id,
				'type' => 'attachment',
				'type_id' => $attachment->id,
				'action' => 'created',
				'content' => $attachment->toJSON()
			));*/
			return Response::json($attachment);
		} else {
			return Response::json(array('code' => '412', 'message' => $attachment->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comments = TaskComment::all();
		$comments = $comments->filter(function($comment) use ($tasks){
			return ($tasks->contains($comment->task_id));
		})->values();

		$attachment = TaskCommentAttachment::where('id', $id)->with(array('comment', 'uploadedByUser'))->firstOrFail();
		if(!$comments->contains($attachment->comment_id)) return App::abort(403, 'Unauthorized.');

		return Response::json($attachment);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comments = TaskComment::all();
		$comments = $comments->filter(function($comment) use ($tasks){
			return ($tasks->contains($comment->task_id));
		})->values();

		$attachment = TaskCommentAttachment::where('id', $id)->firstOrFail();
		if(!$comments->contains($attachment->comment_id)) return App::abort(403, 'Unauthorized.');

		if($attachment->update(Input::all())){
			$attachment->load(array('comment', 'uploadedByUser'));

			/*History::create(array(
				'project_id' => $attachment->comment->task->project->id,
				'user_id' => $this->user->id,
				'type' => 'attachment',
				'type_id' => $attachment->id,
				'action' => 'updated',
				'content' => $attachment->toJSON()
			));*/
			return Response::json($attachment);
		} else {
			return Response::json(array('code' => '412', 'message' => $attachment->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$projects = User::find($this->user->id)->projects;
		$tasks = Task::all();
		$tasks = $tasks->filter(function($task) use ($projects){
			return ($projects->contains($task->project_id));
		})->values();

		$comments = TaskComment::all();
		$comments = $comments->filter(function($comment) use ($tasks){
			return ($tasks->contains($comment->task_id));
		})->values();

		$attachment = TaskCommentAttachment::where('id', $id)->firstOrFail();
		if(!$comments->contains($attachment->comment_id)) return App::abort(403, 'Unauthorized.');

		/*History::create(array(
			'project_id' => $attachment->comment->task->project->id,
			'user_id' => $this->user->id,
			'type' => 'attachment',
			'type_id' => $attachment->id,
			'action' => 'deleted',
			'content' => $attachment->toJSON()
		));*/
		return Response::json($attachment->delete());
	}

}
