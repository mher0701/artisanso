<?php

class ApiController extends Controller {

	protected $user;

	public function __construct()
	{
		$user = null;
		$api_key = Request::header('X-API-KEY');
		if($api_key) $user = User::where('api_key', $api_key)->first();

		if(!$user){
			header('Content-Type: application/json');
			header('HTTP/1.1 401 Unauthorized');
			echo json_encode(array(
				'error' => array(
					'code' => 401,
					'message' => 'Unauthorized'
				)
			), 401);
			exit;
		} else {
			$this->user = $user;
		}
	}

}
