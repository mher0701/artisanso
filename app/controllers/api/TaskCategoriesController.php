<?php

class TaskCategoriesController extends \ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = User::find($this->user->id)->projects;
		$categories = TaskCategory::orderBy('project_id')->get();

		$categories = $categories->filter(function($category) use ($projects){
			return $projects->contains($category->project_id);
		})->values();

		if(Input::get('project_id')){
			$categories = $categories->filter(function($category){
				return $category->project_id == Input::get('project_id');
			})->values();
		}

		return Response::json($categories);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$projects = User::find($this->user->id)->projects;
		if(!$projects->contains(Input::get('project_id'))){
			return Response::json(array('code' => '412', 'message' => 'Invalid project_id'), 412);
		}

		$category = new TaskCategory(Input::all());
		if($category->save()){
			return Response::json($category);
		} else {
			return Response::json(array('code' => '412', 'message' => $category->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$projects = User::find($this->user->id)->projects;
		$category = TaskCategory::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($category->project_id)) return App::abort(403, 'Unauthorized.');
		return Response::json($category);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projects = User::find($this->user->id)->projects;
		$category = TaskCategory::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($category->project_id)) return App::abort(403, 'Unauthorized.');

		if($category->update(Input::all())){
			return Response::json($category);
		} else {
			return Response::json(array('code' => '412', 'message' => $category->getErrors()->toArray()), 412);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$projects = User::find($this->user->id)->projects;
		$category = TaskCategory::where('id', $id)->with(array('project'))->firstOrFail();
		if(!$projects->contains($category->project_id)) return App::abort(403, 'Unauthorized.');

		return Response::json($category->delete());
	}

}
