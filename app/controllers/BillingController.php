<?php

class BillingController extends \BaseController {

	public function index()
	{
		if(Auth::user()->role == 'team') return App::abort(404);

		$invoices = Auth::user()->invoices();

		return View::make('app.billing')->with(array(
			'invoices' => $invoices,
			'page_title' => 'Billing'
		));
	}

	public function upgrade($plan)
	{
		if(Auth::user()->role == 'team') return App::abort(404);
		if($plan != 'startup' && $plan != 'agency' && $plan != 'business') App::abort(404);

		return View::make('app.upgrade')->with(array(
			'plan' => $plan,
			'page_title' => 'Upgrade'
		));
	}

	public function processUpgrade($plan)
	{
		$user = Auth::user();
		if($user->stripe_plan != ''){
			try {
				$user->subscription('artisan_'. $plan)->swap();
			}
			catch(Exception $e){
				return Redirect::to('billing/upgrade/'. $plan)->withError($e->getMessage());
			}
		} else {
			$validator = Validator::make(array('token' => Input::get('token')), array(
				'token' => 'required'
			));
			if($validator->fails()){
				return Redirect::to('billing/upgrade/'. $plan)->withError('Failed to process payment.');
			}

			try {
				if($user->stripe_plan == 'artisan_'. $plan && $user->onGracePeriod()){
					$user->subscription('artisan_'. $plan)->resume(Input::get('token'));
				} else {
					$user->subscription('artisan_'. $plan)->create(Input::get('token'));
				}
			}
			catch(Exception $e){
				return Redirect::to('billing/upgrade/'. $plan)->withError($e->getMessage());
			}
		}

		Mail::send('emails.upgrade', array('user' => $user, 'plan' => $plan), function($message) use ($user, $plan){
			$message->to($user->email, $user->name)
			->from('hi@artisan.so', 'Artisan')
			->subject('You have upgraded to the '. ucfirst($plan) .' plan on Artisan');
		});

		return Redirect::to('billing')->withMessage('You have successfully upgraded to the '. ucfirst($plan) .' plan.');
	}

	public function edit()
	{
		if(!Auth::user()->subscribed()) App::abort(404);

		return View::make('app.billing-edit')->with(array('page_title' => 'Edit Billing Details'));
	}

	/*public function processEdit()
	{
		$user = Auth::user();
		$validator = Validator::make(array('token' => Input::get('token')), array(
			'token' => 'required'
		));
		if($validator->fails()){
			return Redirect::to('billing/edit')->withError('Failed to save details.');
		}

		try {

		}
		catch(Exception $e){
			return Redirect::to('billing/edit')->withError($e->getMessage());
		}

		return Redirect::to('billing')->withMessage('You have successfully updated your billing details');
	}*/

	public function processCancel()
	{
		$user = Auth::user();
		if($user->stripe_plan != ''){
			try {
				$user->subscription()->cancel();
			}
			catch(Exception $e){
				return Redirect::to('billing/edit')->withError($e->getMessage());
			}

			Mail::send('emails.cancel', array('user' => $user), function($message) use ($user){
				$message->to($user->email, $user->name)
				->from('hi@artisan.so', 'Artisan')
				->subject('You have cancelled your plan on Artisan');
			});
		}

		return Redirect::to('billing');
	}

	public function invoice($id)
	{
		return Auth::user()->downloadInvoice($id, array(
		    'vendor'  => 'Dev7studios Ltd, 3 Duffus Place, Elgin, IV30 5PB',
		    'product' => 'Artisan'
		));
	}

}
