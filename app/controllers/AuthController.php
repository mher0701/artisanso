<?php

class AuthController extends \BaseController {

	public function login()
	{
		return View::make('app.login')->with(array('page_title' => 'Login'));
	}

	public function processLogin()
	{
		$data = array(
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);

		$validator = Validator::make($data, array(
			'email' => 'required|email',
			'password' => 'required'
		));
	    if($validator->fails()){
			Input::flashOnly('email');
			return Redirect::to('login')->withErrors($validator);
		}

		if(Auth::attempt($data)){
			return Redirect::to('/');
		} else {
			Input::flashOnly('email');
			return Redirect::to('login')->withError('Invalid email address or password.');
		}
	}

	public function signup()
	{
		return View::make('app.signup')->with(array('page_title' => 'Sign Up'));
	}

	public function processSignup()
	{
		$validator = Validator::make(Input::all(), array(
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|confirmed'
		));
		if($validator->fails()){
			Input::flashOnly('name', 'email');
			return Redirect::to('signup')->withErrors($validator);
		}

		$user = User::firstOrNew(Input::only('name', 'email', 'password'));
		$user->api_key = sha1($user->id . $user->email . time());
		if($user->save()){
			// Mail::send('emails.signup', array('user' => $user), function($message) use ($user){
			//     $message->to($user->email, $user->name)
			// 	->from('hi@artisan.so', 'Artisan')
			// 	->subject('Welcome to Artisan');
			// });

			Auth::login($user);
		} else {
			return Redirect::to('signup')->withError('Invalid name, email address or password.');
		}

		return Redirect::to('/');
	}

	public function signupClosed()
	{
		return Redirect::to('signup'); // Post beta

		return View::make('app.signup_closed')->with(array('page_title' => 'Sign Up Closed'));
	}

	public function inviteSignup($token)
	{
		$hashids = new Hashids();
		$id = $hashids->decrypt($token);
		if(isset($id[0])){
			$id = $id[0];
		} else {
			App::abort(404);
		}
		$user = User::findOrFail($id);
		if($user->status != 'invited') return Redirect::to('signup');

		return View::make('app.invite_signup')->with(array(
			'user' => $user,
			'token' => $token,
			'page_title' => 'Sign Up'
		));
	}

	public function processInviteSignup($token)
	{
		$hashids = new Hashids();
		$id = $hashids->decrypt($token);
		if(isset($id[0])){
			$id = $id[0];
		} else {
			App::abort(404);
		}
		$user = User::findOrFail($id);
		if($user->status != 'invited') return Redirect::to('signup');

		$validator = Validator::make(Input::all(), array(
			'name' => 'required',
			'password' => 'required|confirmed'
		));
		if($validator->fails()){
			return Redirect::to('signup/invite/'. $token)->withErrors($validator);
		}

		$user->status = 'active';
		if($user->update(Input::only('name', 'password'))){
			// Mail::send('emails.signup', array('user' => $user), function($message) use ($user){
			// 	$message->to($user->email, $user->name)
			// 	->from('hi@artisan.so', 'Artisan')
			// 	->subject('Welcome to Artisan');
			// });

			Auth::login($user);
		} else {
			return Redirect::to('signup/invite/'. $token)->withErrors($user->getErrors());
		}

		return Redirect::to('/');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('login');
	}

	public function startFreeTrial()
	{
		$user = Auth::user();
		if($user->role == 'team'){
			$user->role = 'user';
			if(!$user->used_free_trial){
				$user->trial_ends_at = \Carbon\Carbon::now()->addDays(14);
				$user->used_free_trial = true;
			}
			$user->save();

			// Mail::send('emails.free-trial', array('user' => $user), function($message) use ($user){
			// 	$message->to($user->email, $user->name)
			// 	->from('hi@artisan.so', 'Artisan')
			// 	->subject('Your Artisan Free Trial has Started');
			// });
		}

		return Redirect::to('app');
	}

	public function downgradeToTeam()
	{
		$user = Auth::user();
		if($user->role == 'user'){
			$user->role = 'team';
			$user->save();
		}

		return Redirect::to('app');
	}

}
