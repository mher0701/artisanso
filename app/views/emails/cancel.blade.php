<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi {{ $user->name }},<br>
			<br>
			You have cancelled your plan on Artisan. You will no longer be charged for using Artisan and
			your account will remain active until the end of the current billing cycle. Once your account
			is inactive you will have 30 days to upgrade before your data will be permenantly deleted.<br>
			<br>
			Resume your subscription at: {{ URL::to('billing') }}<br>
			<br>
			If you ever need any help just reply to this email.<br>
			<br>
			Thanks,<br>
			Team Artisan
		</div>
	</body>
</html>
