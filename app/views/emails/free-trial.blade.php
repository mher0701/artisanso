<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			Hi {{ $user->name }},<br>
			<br>
			Congratulations your Artisan free trial has started!<br>
			<br>
			If you ever need any help just reply to this email.<br>
			<br>
			Thanks,<br>
			Team Artisan
		</div>
	</body>
</html>
