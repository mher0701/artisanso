<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@if (isset($page_title) && $page_title)
    {{{ $page_title }}} - Artisan
    @else
    Artisan
    @endif</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ url('favicon.ico') }}" />
    <meta name="robots" content="noindex,nofollow">

    <link rel="stylesheet" href="{{ url('styles/app.css') }}">

    <script type="text/javascript" src="//use.typekit.net/inm0gtw.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    @if (App::environment('production'))
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-3892196-62', 'artisan.so');
    ga('send', 'pageview');
    </script>
    @endif
</head>
<body>

    <header class="header">
        @if (Auth::check())
        <ul class="settings-nav">
            @if (Auth::user()->role == 'user' && Auth::user()->stripe_plan == '' && Auth::user()->onTrial())
            <li class="free-trial-expires"><a href="{{ url('billing') }}">Free trial expires on {{ date('j F Y', strtotime(Auth::user()->trial_ends_at)) }}</a></li>
            @endif
            @if (Auth::user()->role == 'user' && Auth::user()->stripe_plan == '' && !Auth::user()->onTrial())
            <li class="free-trial-expires"><a href="{{ url('billing') }}">Free trial expired. Upgrade now!</a></li>
            @endif
            <li class="feedback"><a href="mailto:support@artisan.so?Subject=Artisan%20Feedback">Give us Feeback</a></li>
            <li class="settings">
                <a href="#" class="icon-cog" title="Settings"></a>
                <ul>
                    <li class="account{{ Request::is('account') ? ' active' : '' }}"><a href="{{ url('account') }}">Account</a></li>
                    @if (Auth::user()->role != 'team')
                    <li class="team{{ Request::is('team') ? ' active' : '' }}"><a href="{{ url('team') }}">Team</a></li>
                    <li class="billing{{ Request::is('billing') ? ' active' : '' }}"><a href="{{ url('billing') }}">Billing</a></li>
                    <li class="support"><a href="mailto:support@artisan.so">Support</a></li>
                    @endif
                    @if (Auth::user()->role == 'admin')
                    <li class="admin{{ Request::is('admin') ? ' active' : '' }}"><a href="{{ url('admin') }}">Admin</a></li>
                    @endif
                    <li class="logout"><a href="{{ url('logout') }}">Logout</a></li>
                </ul>
            </li>
        </ul>
        @endif
        <a href="{{ url('/') }}" class="title">Artisan</a>
        @yield('header')
    </header>

    <section id="content">
        @yield('content')
    </section>

    <script src="{{ url('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ url('bower_components/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
    <script src="{{ url('bower_components/dropit/dropit.js') }}"></script>
    <script src="{{ url('scripts/global.js') }}"></script>
    @yield('footer')

    @if (Auth::check() && App::environment('production'))
    <script id="IntercomSettingsScriptTag">
    window.intercomSettings = {
        app_id: "evt4aok",
        user_hash: "{{ hash_hmac('sha256', Auth::user()->id, 'MsPBlcAbHc31_nhnQDY0PGkjZOnJdurPAkfOH09f') }}",
        user_id: "{{ Auth::user()->id }}",
        name: "{{ Auth::user()->name }}",
        email: "{{ Auth::user()->email }}",
        role: "{{ Auth::user()->role }}",
        status: "{{ Auth::user()->status }}",
        created_at: {{ strtotime(Auth::user()->created_at) }},
        stripe_active: {{ Auth::user()->stripe_active }},
        stripe_plan: "{{ Auth::user()->stripe_plan }}",
        trial_ends_at: {{ Auth::user()->trial_ends_at ? strtotime(Auth::user()->trial_ends_at) : 0 }},
        subscription_ends_at: {{ Auth::user()->subscription_ends_at ? strtotime(Auth::user()->subscription_ends_at) : 0 }},
        used_free_trial: {{ Auth::user()->used_free_trial }}
    };
    </script>
    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://static.intercomcdn.com/intercom.v1.js';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
    @endif

</body>
</html>
