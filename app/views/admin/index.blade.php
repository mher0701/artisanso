@extends('layout')

@section('content')

	<div id="admin">
		<h1>Admin</h1>
		<div id="users-table"></div>
		<a href="#" id="add-user">Add User +</a>
	</div>

@stop

@section('footer')

	<link rel="stylesheet" href="{{ url('bower_components/backgrid/lib/backgrid.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ url('bower_components/backgrid-paginator/backgrid-paginator.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ url('bower_components/backgrid-filter/backgrid-filter.css') }}" type="text/css">
	<script>
	var currentUser = {{ Auth::user()->toJSON() }};
	currentUser.apiKey = "{{ Auth::user()->getAuthApiKey() }}";
	</script>
	<script src="{{ url('bower_components/underscore/underscore.js') }}"></script>
	<script src="{{ url('bower_components/backbone/backbone.js') }}"></script>
	<script src="{{ url('bower_components/handlebars/handlebars.js') }}"></script>
	<script src="{{ url('bower_components/Backbone.Handlebars/lib/backbone_handlebars.js') }}"></script>
	<script src="{{ url('bower_components/backbone-forms/distribution/backbone-forms.min.js') }}"></script>
	<script src="{{ url('bower_components/backbone-modal/backbone.modal.js') }}"></script>
	<script src="{{ url('bower_components/backgrid/lib/backgrid.js') }}"></script>
	<script src="{{ url('bower_components/backbone-pageable/lib/backbone-pageable.js') }}"></script>
	<script src="{{ url('bower_components/backgrid-paginator/backgrid-paginator.js') }}"></script>
	<script src="{{ url('bower_components/backgrid-filter/backgrid-filter.js') }}"></script>
	<script src="{{ url('scripts/setup.min.js') }}"></script>
	<script src="{{ url('scripts/admin.js') }}"></script>

@stop
