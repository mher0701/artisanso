@extends('layout')

@section('content')

	<div id="forgot-password">
		<h1>Forgot Password</h1>
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
		{{ Session::get('status') ? '<p class="success">'. Session::get('status') .'</p>' : '' }}
		{{ Form::open(array('action' => 'RemindersController@postRemind')) }}
			{{ Form::label('email', 'Email Address') }}<br>
			{{ Form::text('email', Input::old('email')) }}<br>
			{{ Form::submit('Send Reminder', array('class' => 'btn')) }}
			{{ link_to('login', 'Back to login', array('class' => 'back-to-login')) }}
		{{ Form::close() }}
	</div>

@stop
