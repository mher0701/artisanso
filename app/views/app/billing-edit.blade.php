@extends('layout')

@section('header')

	<a href="/" class="project-select">All Projects</a>

@stop

@section('content')

	<div id="billing-edit">
		<h1>Edit Billing Details</h1>
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}

		<p class="cancel-intro">Cancelling your subscription will disable
		your account until you decide to upgrade again. Note that we only keep your data for
		30 days after you have cancelled your account.</p>
		{{ Form::open(array('url' => 'billing/cancel', 'id' => 'billing-cancel-form')) }}
			<a href="/billing" class="return">Return to Billing</a>
			{{ Form::submit('Cancel Subscription', array('class' => 'btn danger')) }}
		{{ Form::close() }}
	</div>

@stop

@section('footer')

	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
	Stripe.setPublishableKey('{{ App::environment('production') ? 'pk_4hEnTvmFUri6mEGyquHg5rf3CKeZ5' : 'pk_test_eL1sQ3wNh3R6YhLytkwz1XvT' }}');
	</script>
	<script src="{{ url('scripts/upgrade.js') }}"></script>

@stop
