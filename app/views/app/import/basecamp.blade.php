@extends('layout')

@section('header')

	<a href="/" class="project-select">All Projects</a>

@stop

@section('content')

	<div id="import" class="basecamp">
		<h1>Import from Basecamp</h1>
		@if (isset($accounts))
		{{ Form::open(array('url' => 'import/basecamp/go', 'id' => 'import-form')) }}
			{{ Form::label('account', 'Please select an account to import') }}
			<div class="styled-select">
			{{ Form::select('account', $accounts) }}
			</div>
			{{ Form::submit('Start Import', array('class' => 'btn')) }}
		{{ Form::close() }}
		@else
		{{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
		<p>Once you have signed into Basecamp you will be able to choose an account to import from. Artisan
		will then import all Projects, Todolist's and Todo's from Basecamp.</p>
		<a href="{{ URL::to('import/basecamp/login') }}" class="btn">Sign in to Basecamp</a>
		@endif
	</div>

@stop

@section('footer')

	<script src="{{ url('scripts/import.js') }}"></script>

@stop
