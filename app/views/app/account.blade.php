@extends('layout')

@section('header')

    <a href="/" class="project-select">All Projects</a>

@stop

@section('content')

    <div id="account">
        <h1>Manage Account</h1>
        <img src="{{ Gravatar::src($user->email, 100) }}" alt="" class="avatar">
        {{ Session::get('error') ? '<p class="error">'. Session::get('error') .'</p>' : '' }}
        {{ Session::get('status') ? '<p class="success">'. Session::get('status') .'</p>' : '' }}
        {{ Form::model($user, array('url' => 'account/info')) }}
            {{ Form::label('name', 'Name') }}
            {{ $errors->first('name') ? '<span class="error">'. $errors->first('name') .'</span>' : '' }}<br>
            {{ Form::text('name') }}<br>
            {{ Form::label('email', 'Email Address') }}
            {{ $errors->first('email') ? '<span class="error">'. $errors->first('email') .'</span>' : '' }}<br>
            {{ Form::text('email') }}<br>
            {{ Form::submit('Save', array('class' => 'btn')) }}
        {{ Form::close() }}

        {{ Form::model($user, array('url' => 'account/password')) }}
            {{ Form::label('password', 'New Password') }}
            {{ $errors->first('password') ? '<span class="error">'. $errors->first('password') .'</span>' : '' }}<br>
            {{ Form::password('password') }}<br>
            {{ Form::label('password_confirmation', 'Confirm Password') }}
            {{ $errors->first('password_confirmation') ? '<span class="error">'. $errors->first('password_confirmation') .'</span>' : '' }}<br>
            {{ Form::password('password_confirmation') }}<br>
            {{ Form::submit('Save', array('class' => 'btn')) }}
        {{ Form::close() }}
    </div>

@stop
