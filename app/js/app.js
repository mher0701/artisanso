(function(App, $, Backbone){

    $(function(){

        /**
         * Routes
         */
        App.Router.on('route:index', function() {
            $('#content').html('Loading...');
        });

        App.Router.on('route:projects', function(id) {
            $('#content').html('');
            new App.Views.ProjectsView({ collection: App.Global.Projects });
        });

        App.Router.on('route:project', function(id) {
            $('#content').html('');
            var model = new App.Models.Project({ id: id });
            new App.Views.ProjectView({ model: model });
            $('.header .project-select option[value="'+ id +'"]').attr('selected', true);
        });

        App.Router.on('route:milestone', function(id) {
            $('#content').html('');
            var model = new App.Models.Milestone({ id: id });
            new App.Views.MilestoneView({ model: model });
        });

        App.Router.on('route:task', function(id) {
            $('#content').html('');
            var model = new App.Models.Task({ id: id });
            new App.Views.TaskView({ model: model });
        });

        App.Router.on('route:activity', function(id) {
            $('#content').html('');
            new App.Views.HistoryView();
        });

        /**
         * Misc Views
         */
        App.Views.ProjectSelectView = Backbone.View.extend({

            className: 'project-select',
            template: App.Templates.ProjectSelect,

            events: {
                'change select': 'changeProject'
            },

            initialize: function() {
                this.listenTo(App.Global.Projects, 'add', this.render);
                this.listenTo(App.Global.Projects, 'change', this.render);
                this.listenTo(App.Global.Projects, 'remove', this.render);

                App.Global.Events.on('update-project-select', this.updateSelect, this);
            },

            render: function(){
                this.renderTemplate({ projects: App.Global.Projects.toJSON() });
                if(this.currentProject){
                    this.$el.find('option[value="'+ this.currentProject.id +'"]').attr('selected', true);
                }
                return this;
            },

            changeProject: function(){
                var selectedProject = this.$el.find('option:selected').val();
                if(selectedProject == 'all'){
                    this.currentProject = null;
                    App.Router.navigate('#/projects', { trigger: true });
                } else {
                    this.currentProject = App.Global.Projects.get(selectedProject);
                    App.Router.navigate('#/project/'+ this.currentProject.id, { trigger: true });
                }
            },

            updateSelect: function(data){
                if(typeof data.project_id !== 'undefined'){
                    this.currentProject = App.Global.Projects.get(data.project_id);
                    this.$el.find('option[value="'+ this.currentProject.id +'"]').attr('selected', true);
                }
            }

        });

        App.Views.UpgradeModal = Backbone.Modal.extend({

            className: 'upgrade-modal',
            template: App.Templates.UpgradeModal,

            events: {
                'click .cancel': 'hideModal'
            },

            initialize: function(atts) {
                this.message = atts.message;
            },

            onRender: function() {
                this.$el.find('.modal-content').html('<p class="message">'+ this.message +'</p>');
            },

            hideModal: function(e) {
                e.preventDefault();
                this.triggerCancel();
            }

        });

        App.Views.NotFoundView = Backbone.View.extend({

            className: 'not-found',
            template: App.Templates.NotFound

        });

        /**
         * Main View
         */
        App.Views.MainView = Backbone.View.extend({

            initialize: function() {
                _.bindAll(this, 'projectsLoaded');

                App.Global.Projects = new App.Collections.Projects({ model: App.Models.Project });
                App.Global.Projects.fetch({
                    success: this.projectsLoaded,
                });

                App.Global.CurrentUser = new App.Models.User(currentUser);

                new App.Views.ActivityMenuView();
            },

            projectsLoaded: function(){
                Backbone.history.start();

                var route = Backbone.history.fragment;
                if(!route) route = '#/projects';

                var projectSelect = new App.Views.ProjectSelectView();
                $('.header').append(projectSelect.render().el);
                App.Router.navigate(route, { trigger: true });
            }

        });
        new App.Views.MainView();

    });

}(App, jQuery, Backbone));
