App.Views.MilestoneListView = Backbone.View.extend({

	tagName: 'li',
	className: 'milestone',
	template: App.Templates.MilestoneList,

	initialize: function() {
		this.listenTo(this.model, 'change', this.render);
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		this.$el.attr('data-id', this.model.get('id')); // For sorting
		var view = new App.Views.MilestoneTasksView({ model: this.model });
		this.$el.find('.milestone-body').append(view.render().el);
		return this;
	}

});

App.Views.CreateMilestoneView = Backbone.View.extend({

	tagName: 'li',
	className: 'milestone-create cf',
	template: App.Templates.MilestoneCreate,

	events: {
		'click .create-milestone': 'showForm',
		'click .cancel': 'hideForm',
		'click .submit': 'createMilestone',
		'submit form': 'submit'
	},

	initialize: function(atts) {
		this.project = atts.project;
		this.milestones = atts.milestones;
	},

	render: function() {
		this.renderTemplate();
		this.form = new Backbone.Form({
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Milestone name (e.g. "v1.0" or "Sprint 1")' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
			}
		}).render();
		this.$el.find('.milestone-form').html(this.form.el);
		return this;
	},

	showForm: function(e) {
		e.preventDefault();
		this.$el.addClass('active');
	},

	hideForm: function() {
		this.$el.removeClass('active');
	},

	reset: function() {
		this.$el.find('form')[0].reset();
	},

	createMilestone: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			data.project_id = this.project.get('id');
			this.milestones.create(data, { wait: true });
			this.reset();
			this.hideForm();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.MilestoneView = Backbone.View.extend({

	id: 'milestone',
	className: 'milestone',
	template: App.Templates.Milestone,

	events: {
		'click .milestone-edit': 'editMilestone',
		'click .milestone-delete': 'deleteMilestone'
	},

	initialize: function() {
		var that = this;
		this.$el.appendTo('#content');

		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.removeMilestone);

		this.model.fetch({
			success: function(){
				App.Global.Events.trigger('update-project-select', { project_id: that.model.get('project_id') });
			},
			error: function(){
				var view = new App.Views.NotFoundView();
				$('#content').html(view.render().el);
			}
		});
	},

	render: function(){
		this.renderTemplate(this.model.toJSON());
		var view = new App.Views.MilestoneTasksView({ model: this.model });
		this.$el.find('.milestone-body').append(view.render().el);
		return this;
	},

	editMilestone: function(e) {
		e.preventDefault();
		var modal = new App.Views.EditMilestoneModal({ model: this.model });
		$('body').append(modal.render().el);
	},

	deleteMilestone: function(e) {
		e.preventDefault();
		if(confirm('Are you sure you want to permenantly delete this milestone?')){
			this.projectId = this.model.get('project_id');
			this.model.destroy({ wait: true });
		}
	},

	removeMilestone: function() {
		this.remove();
		App.Router.navigate('#/project/'+ this.projectId, { trigger: true });
	}

});

App.Views.EditMilestoneModal = Backbone.Modal.extend({

	className: 'milestone-edit-modal',
	template: App.Templates.MilestoneEditModal,

	events: {
		'click .cancel': 'hideModal',
		'click .submit': 'saveMilestone',
		'submit form': 'submit'
	},

	onRender: function() {
		this.form = new Backbone.Form({
			model: this.model,
			schema: {
				name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Milestone name' } },
				description: { type: 'TextArea', editorAttrs: { placeholder: 'Description (optional)' } }
			}
		}).render();
		this.$el.find('.modal-content').html(this.form.el);
	},

	hideModal: function() {
		this.triggerCancel();
	},

	saveMilestone: function() {
		if(!this.form.validate()){
			var data = this.form.getValue();
			this.model.save(data, { wait: true });
			this.hideModal();
		}
	},

	submit: function(e) {
		e.preventDefault();
	}

});

App.Views.MilestoneTasksView = Backbone.View.extend({

	tagName: 'ul',
	className: 'tasks',

	initialize: function(atts) {
		this.tasks = new App.Collections.Tasks({ model: App.Models.Task });

		this.listenTo(this.tasks, 'add', this.addOne);
		this.listenTo(this.tasks, 'reset', this.addAll);

		this.tasks.fetch({ data: { milestone_id: this.model.id } });

		var that = this;
		this.$el.sortable({
			items: 'li.task',
			revert: 100,
			axis: 'y',
			containment: 'parent',
			update: function(event, ui) {
				that.updateSort();
			}
		});
	},

	render: function() {
		var view = new App.Views.CreateTaskView({ milestone: this.model, tasks: this.tasks });
		this.$el.append(view.render().el);
		return this;
	},

	addOne: function(model) {
		var view = new App.Views.TaskListView({ model: model });
		if(model.get('completed') == false){
			view.render().$el.insertBefore(this.$el.find('.task-create'));
		} else {
			this.$el.append(view.render().el);
		}
	},

	addAll: function() {
		this.$el.html('');
		this.tasks.each(this.addOne, this);
	},

	updateSort: function() {
		var tasks = this.tasks;
			sorted = this.$el.sortable('toArray', { attribute: 'data-id' });

		_.each(sorted, function(id, index){
			var task = tasks.get(id);
			task.save({ 'sort': index });
		});
	}

});
