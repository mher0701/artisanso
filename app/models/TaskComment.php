<?php

class TaskComment extends WayModel {

	protected $fillable = array(
		'task_id',
		'user_id',
		'content'
	);

	protected $touches = array('task');

	protected static $rules = array(
		'task_id' => 'required',
		'user_id' => 'required',
		'content' => 'required'
	);

	public function task()
	{
		return $this->belongsTo('Task');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

	public function attachments()
	{
		return $this->hasMany('TaskCommentAttachment', 'comment_id');
	}

	public function delete()
	{
		$this->attachments()->delete();
		return parent::delete();
	}

	public function getContentAttribute($value)
    {
		$value = Markdown::string($value);
		$value = preg_replace('/([^>])\n{1}([^<])/i', '$1<br>$2', $value); // Single newline breaks
        return $value;
    }

}
