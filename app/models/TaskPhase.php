<?php

class TaskPhase extends WayModel {

	protected $fillable = array(
		'project_id',
		'name',
		'description'
	);

	protected static $rules = array(
		'project_id' => 'required',
		'name' => 'required'
	);

	public function project()
	{
		return $this->belongsTo('Project');
	}

}
