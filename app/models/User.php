<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;

class User extends WayModel implements UserInterface, RemindableInterface, BillableInterface {

	use BillableTrait;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
	protected $cardUpFront = false;

	protected $fillable = array(
		'email',
		'password',
		'name',
		'api_key',
		'remember_token',
		'role',
		'status',
		'used_free_trial'
	);

	protected static $rules = array(
		'email' => 'required|email',
		'name' => 'required',
		'password' => 'required'
	);

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'api_key', 'remember_token');

	public function __construct(array $attributes = array())
	{
	    $this->setRawAttributes(array_merge($this->attributes, array(
	    	'trial_ends_at' => \Carbon\Carbon::now()->addDays(14)
	    )), true);
	    parent::__construct($attributes);
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	* Get the API key for the user.
	*
	* @return string
	*/
	public function getAuthApiKey()
	{
		return $this->api_key;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}

	public function setPasswordAttribute($pass)
	{
		$this->attributes['password'] = Hash::make($pass);
	}

	public function projects()
	{
		return $this->belongsToMany('Project')->withTimestamps();
	}

	public function project($project_id)
	{
		return $this->belongsToMany('Project')->where('project_user.project_id', $project_id);
	}

}
