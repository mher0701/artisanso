<?php

class Project extends WayModel {

	protected $fillable = array(
		'owner',
		'name',
		'description',
		'sort'
	);

	protected static $rules = array(
        'owner' => 'required',
		'name' => 'required'
    );

	public function milestones()
	{
		return $this->hasMany('Milestone');
	}

	public function tasks()
    {
        return $this->hasMany('Task');
    }

	public function taskCategories()
	{
		return $this->hasMany('TaskCategory');
	}

	public function taskPhases()
	{
		return $this->hasMany('TaskPhase');
	}

	public function users()
	{
		return $this->belongsToMany('User')->withTimestamps();
	}

	public function delete()
	{
		$this->milestones()->delete();
		$this->taskCategories()->delete();
		$this->taskPhases()->delete();
		return parent::delete();
	}

}
