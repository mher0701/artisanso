<?php

class Milestone extends WayModel {

	protected $fillable = array(
		'project_id',
		'name',
		'description',
		'sort'
	);

	protected $touches = array('project');

	protected static $rules = array(
        'project_id' => 'required',
		'name' => 'required'
    );

	public function project()
    {
        return $this->belongsTo('Project');
    }

	public function tasks()
	{
		return $this->hasMany('Task');
	}

	public function delete()
	{
		// TODO Delete task comments
		$this->tasks()->delete();
		return parent::delete();
	}

}
