<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Basecamp
		 */
        'Basecamp' => array(
            'client_id'     => '69925af46c2eb3cbfa58392ce1b7229a4b48ddec',
            'client_secret' => '30192e6a893aa8786f17149910501b467ef2dd07',
            'scope'         => array(),
        ),

	)

);
