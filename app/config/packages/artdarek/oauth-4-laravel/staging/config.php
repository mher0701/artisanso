<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => 'Session',

	/**
	 * Consumers
	 */
	'consumers' => array(

		/**
		 * Basecamp
		 */
        'Basecamp' => array(
            'client_id'     => '8682e907647c1e299c343aecb7524a6550436cf1',
            'client_secret' => '9ef902fd3b3ea63ca5d1c17afb406db1816d039d',
            'scope'         => array(),
        ),

	)

);
