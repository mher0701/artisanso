<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PostDeploy extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'post-deploy';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Post deploy setup.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$env = $this->option('env') ? ' --env='. $this->option('env') : '';

		$this->runExec('php artisan down');
		$this->runExec('composer update');
		$this->runExec('php artisan migrate'. $env);
		$this->runExec('php artisan up');
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('env', null, InputOption::VALUE_OPTIONAL, 'Environment to pass to migrate command')
		);
	}

	/**
	 * Utility function to run exec()
	 *
	 * @return mixed
	 */
	private function runExec($command)
	{
		$this->comment('Running command: '. $command);
		exec($command, $output, $return);
		if(!empty($output)){
			foreach($output as $line){
				if($return !== false){
					$this->info($line);
				} else {
					$this->error($line);
				}
			}
		}
	}

}
