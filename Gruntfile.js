module.exports = function(grunt) {

	// Config
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		concat: {
			dist: {
				files: [
					{
						src: [
							'app/js/setup.js',
							'app/js/templates-jst.js',
							'app/js/templates-handlebars.js',
							'app/js/models/*.js',
							'app/js/collections/*.js',
							'app/js/views/*.js',
							'app/js/app.js'
						],
						dest: 'public/scripts/app.compiled.js',
						nonull: true
					}
				]
			}
		},

		jshint: {
			options: {
				reporter: require('jshint-stylish'),
				'-W041': false // Disable the "Use '===' to compare with"
			},
			dist: {
				files: {
					src: [
						'app/js/**/*.js',
						'!app/js/templates*.js'
					]
				}
			}
		},

		jst: {
			dist: {
				options: {
					namespace: 'App.Templates',
					processName: function(filePath) {
						var pieces = filePath.split('/');
						return _s.classify(pieces[pieces.length - 1].replace('.html',''));
					}
				},
				files: {
					'app/js/templates-jst.js' : [ 'app/js/templates/**/*.html']
				}
			}
		},

		handlebars: {
			dist: {
				options: {
					namespace: 'App.Templates',
					partialsUseNamespace: true,
					partialRegex: /.*/,
					partialsPathRegex: /\/partials\//,
					processName: function(filePath) {
						var pieces = filePath.split('/');
						return _s.classify(pieces[pieces.length - 1].replace('.hbs',''));
					}
				},
				files: {
					'app/js/templates-handlebars.js' : [ 'app/js/templates/**/*.hbs']
				}
			}
		},

		uglify: {
			dist: {
				files: {
					'public/scripts/setup.min.js': 'app/js/setup.js', // For use in public scripts
					'public/scripts/app.min.js': 'public/scripts/app.compiled.js',
				}
			}
		},

		sass: {
			dist: {
				options: {
					style: 'expanded',
					cacheLocation: 'public/styles/.sass-cache'
				},
				files: {
					'public/styles/app.css': 'public/styles/app.scss'
				}
			},
			site: {
				options: {
					style: 'expanded',
					cacheLocation: 'public/styles/.sass-cache'
				},
				files: {
					'public/styles/site.css': 'public/styles/site.scss'
				}
			}
		},

		csslint: {
			options: {
				csslintrc: '.csslintrc'
			},
			dist: {
				files: {
					src: ['public/styles/*.css','!public/styles/site.css']
				}
			},
			site: {
				files: {
					src: ['public/styles/site.css']
				}
			}
		},

		autoprefixer: {
			dist: {
				files: {
					'public/styles/app.css': 'public/styles/app.css'
				}
			},
			site: {
				files: {
					'public/styles/site.css': 'public/styles/site.css'
				}
			}
		},

		cssmin: {
			options: {
				keepSpecialComments: 1,
				report: 'min'
			},
			dist: {
				files: {
					'public/styles/app.min.css': 'public/styles/app.css'
				}
			},
			site: {
				files: {
					'public/styles/site.min.css': 'public/styles/site.css'
				}
			}
		},

		watch: {
			options: {
				livereload: true
			},
			scripts: {
				files: ['app/js/templates/**/*.html', 'app/js/templates/**/*.hbs', 'app/js/**/*.js'],
				tasks: ['jshint:dist', 'jst:dist', 'handlebars:dist', 'concat:dist', 'uglify:dist'],
				options: {
					spawn: false,
				}
			},
			scss: {
				files: ['public/styles/**/*.scss'],
				tasks: ['sass:dist', 'csslint:dist', 'autoprefixer:dist', 'cssmin:dist'],
				options: {
					spawn: false,
				}
			},
			site: {
				files: ['public/styles/**/*.scss'],
				tasks: ['sass:site', 'csslint:site', 'autoprefixer:site', 'cssmin:site'],
				options: {
					spawn: false,
				}
			}
		}

	});

	// Plugins
	var _s = require('underscore.string');
	require('load-grunt-tasks')(grunt);
	grunt.registerTask('forceOn', 'turns the --force option ON', function(){
		if ( !grunt.option( 'force' ) ) {
			grunt.config.set('forceStatus', true);
			grunt.option( 'force', true );
		}
	});
	grunt.registerTask('forceOff', 'turns the --force option OFF', function(){
		if ( grunt.config.get('forceStatus') ) {
			grunt.config.set('forceStatus', false);
			grunt.option( 'force', false );
		}
	});

	// Tasks
	grunt.registerTask('default', [
		'jshint:dist',
		'jst:dist',
		'handlebars:dist',
		'concat:dist',
		'uglify:dist',
		'sass:dist',
		'csslint:dist',
		'autoprefixer:dist',
		'cssmin:dist',
		'watch'
	]);

	grunt.registerTask('site', [
		'sass:site',
		'csslint:site',
		'autoprefixer:site',
		'cssmin:site',
		'watch'
	]);

};
