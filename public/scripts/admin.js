(function(App, $, Backbone){

	App.Views.CreateUserModal = Backbone.Modal.extend({

		className: 'user-create-modal',
		template: _.template('<div class="modal-header"><h3 class="modal-title">Create User</h3></div><div class="modal-content"></div>'),

		events: {
			'click .cancel': 'hideModal',
			'click .submit': 'createUser'
		},

		initialize: function(atts) {
			this.users = atts.users;
		},

		onRender: function() {
			this.form = new Backbone.Form({
				schema: {
					name: { type: 'Text', validators: ['required'], editorAttrs: { placeholder: 'Name' } },
					email: { type: 'Text', validators: ['required','email'], editorAttrs: { placeholder: 'Email Address' } },
					password: { type: 'Password', validators: ['required'], editorAttrs: { placeholder: 'Password' } },
					role: { type: 'Select', options: [{val:'user',label:'User'},{val:'admin',label:'Admin'}], template: selectFieldTemplate },
				}
			}).render();
			this.$el.find('.modal-content').html(this.form.el);
		},

		hideModal: function() {
			this.triggerCancel();
		},

		createUser: function() {
			if(!this.form.validate()){
				var data = this.form.getValue();
				this.users.create(data, { wait: true });
				this.hideModal();
			}
		}

	});

	$(function(){

		var DeleteCell = Backgrid.Cell.extend({
			template: _.template('<a href="#">Delete</a>'),
			events: {
				"click": "deleteRow"
			},
			deleteRow: function (e) {
				e.preventDefault();
				if(confirm('Are you sure you want to permenantly delete this user?')){
					this.model.destroy({ wait: true });
				}
			},
			render: function () {
				this.$el.html(this.template());
				this.delegateEvents();
				return this;
			}
		});

		var User = Backbone.Model.extend({
			urlRoot: App.Config.apiRoot +'/users',
			initialize: function () {
				Backbone.Model.prototype.initialize.apply(this, arguments);
				this.on("change", function(model, options) {
					if (options && options.save === false) return;
					model.save();
				});
			}
		});

		var PageableUsers = Backbone.PageableCollection.extend({
			model: User,
			url: App.Config.apiRoot +'/users',
			state: {
				pageSize: 15
			},
			mode: "client" // page entirely on the client side
		});
		var pageableUsers = new PageableUsers();

		var grid = new Backgrid.Grid({
			columns: [{
					name: "id",
					label: "ID",
					editable: false,
					cell: "string"
				}, {
					name: "name",
					label: "Name",
					cell: "string"
				}, {
					name: "email",
					label: "Email",
					cell: "string"
				}, {
					name: "role",
					label: "Role",
					cell: Backgrid.SelectCell.extend({
						optionValues: [['User','user'],['Admin','admin']]
					})
				}, {
					name: "created_at",
					label: "Created",
					editable: false,
					cell: "date"
				}, {
					name: "delete",
					label: "Delete",
					editable: false,
					cell: DeleteCell
			}],
			collection: pageableUsers
		});

		// Render the grid
		$('#users-table').append(grid.render().el);

		// Initialize the paginator
		var paginator = new Backgrid.Extension.Paginator({
			collection: pageableUsers
		});

		// Render the paginator
		$('#users-table').after(paginator.render().el);

		// Initialize a client-side filter to filter on the client
		// mode pageable collection's cache.
		var filter = new Backgrid.Extension.ClientSideFilter({
			collection: pageableUsers,
			placeholder: "Search for a user",
			fields: ['name','email']
		});

		// Render the filter
		$('#users-table').before(filter.render().el);

		// Add some space to the filter and move it to the right
		$(filter.el).css({float: "right", marginRight: "0"});

		pageableUsers.fetch({reset: true});

		$('#add-user').on('click', function(e){
			e.preventDefault();
			var modal = new App.Views.CreateUserModal({ users: pageableUsers });
			$('body').append(modal.render().el);
		});

	});

}(App, jQuery, Backbone));
