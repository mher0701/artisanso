(function($){

	$(function(){

		$('#project-select').on('change', function(){
			window.location = '/team/'+ $(this).val();
		});

		$('.remove-user').on('click', function(e){
			e.preventDefault();
			var link = $(this);
			if(confirm(link.attr('data-confirm'))){
				$.post('/team/remove-user', { project_id: link.attr('data-project-id'), user_id: link.attr('data-user-id') }, function(data){
					if(data.success){
						$('#user-'+ link.attr('data-user-id')).remove();
					}
				}, 'json');
			}
		});

	});

}(jQuery));
